"""DjangoWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
#from crowd_funding_app import views
from program import views as program_views # ga yakin
from program.views import * #
# from front_end.views import dashboard

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('front_end.urls')),
    path('program', program_views.index_list_program, name = "program"),
    path('news', program_views.index_list_berita, name = "berita")
]
