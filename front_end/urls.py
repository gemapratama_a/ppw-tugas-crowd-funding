from django.urls import path
from front_end import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('login', views.login, name="login")
]