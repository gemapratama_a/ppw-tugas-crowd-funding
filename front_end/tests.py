from django.test import TestCase, Client
from django.urls import resolve
from .views import dashboard

# tests tdd
class TDDTest(TestCase):
    def test_front_end_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_front_end_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'dashboard.html')

    def test_tdd_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, dashboard)