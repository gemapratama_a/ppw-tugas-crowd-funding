from django.db import models

# Untuk di database

class Program(models.Model):
    nama_program      = models.CharField(max_length = 200)
    link_gambar       = models.CharField(max_length = 200)
    deskripsi_program = models.CharField(max_length = 1000)

    def __str__(self):
        return self.nama_program


class Berita(models.Model):
    judul_berita     = models.CharField(max_length = 200)    
    link_gambar_berita    = models.CharField(max_length = 200)
    deskripsi_berita = models.CharField(max_length = 1000)

    def __str__(self):
        return self.judul_berita
