from django.contrib import admin
from .models import Program, Berita

admin.site.register(Program)
admin.site.register(Berita)
