from django.shortcuts import render
from program.models import Program, Berita

# Buat munculin program" di html
def index_list_program(request):
    list_of_programs = Program.objects.all()
    args = {'list_of_programs' : list_of_programs}
    #return render(request, 'program.html' , args)
    return render(request, 'program/program.html' , args)

	
def index_list_berita(request):
	list_of_berita = Berita.objects.all()
	args = {'list_of_berita' : list_of_berita}
	return render(request, 'program/news.html', args)